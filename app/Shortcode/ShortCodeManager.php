<?php
namespace App\Shortcode;

class ShortcodeManager{

    public static $shortcodes = []; // Shortcode List

    // parse content and call shortcode
    public static function parse($content){

        //regex pattren
        $re = '/(?P<shortcode>(?:(?:\s?\[))(?P<name>[\w\.\-]{3,})(?:\s(?P<attrs>[\w\d,\s=\"\'\-\+\#\%\!\~\`\&\.\s\:\/\?\|]+))?(?:\])(?:(?P<html>[\w\d\,\!\@\#\$\%\^\&\*\(\\\\)\s\=\"\'\-\+\&\.\s\:\/\?\|\<\>]+)(?:\[\/[\w\-\_]+\]))?)/m';
        
        $str = $content;
        
        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

        if( isset($matches[0]) && !empty($matches[0]) ){

            foreach ($matches as $key => $value) {
                
                $shortcode =  $value['shortcode'] ?? false;
                $shortcode_name =  $value['name'] ?? false;
                $shortcode_attributes =  $value['attrs'] ?? [];
                $attributes_parse = [];

                if( ( !$shortcode && !$shortcode_name ) ){ continue; }

                if( !empty($shortcode_attributes) ){
                    $attributes_parse = new \SimpleXMLElement("<element $shortcode_attributes />");
                
                    $attributes_parse = (array) $attributes_parse->attributes();
                    $attributes_parse = $attributes_parse['@attributes'];
                }
                
                
                $shortcode_controller = self::getRegisterdShortcodes()[$shortcode_name] ?? false;
        
                $render = $shortcode_controller->render($attributes_parse);
        
                $content = str_replace($shortcode,$render,$content);

            }

        }

        return $content;
    }

    // Add shortcode to the shortcodes list
    public static function register($code, ShortcodeManager $shortcode){
        self::$shortcodes[$code]= $shortcode;
    }

    // get shortcodes list
    public static function getRegisterdShortcodes(){
        return self::$shortcodes;
    }

}