<?php
namespace App\Shortcode\List;
use App\Shortcode\ShortcodeManager;
use App\Shortcode\Shortcode;

class GoodText extends ShortcodeManager implements Shortcode
{
    public string $code = 'good_text'; // shortcode code name : [good_image name="" url=""]

    // get code
    public function getCode():string{
        return $this->code;
    }

    // controller & renderer
    public function render($attrs):string{
        $arr = [
            'text' => $attrs['text']
        ];
        $view = view('shortcode.good_text',$arr);
        return $view->render();
    }
}
