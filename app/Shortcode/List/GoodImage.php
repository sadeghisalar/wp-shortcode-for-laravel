<?php
namespace App\Shortcode\List;
use App\Shortcode\ShortcodeManager;
use App\Shortcode\Shortcode;

class GoodImage extends ShortcodeManager implements Shortcode
{
    public string $code = 'good_image'; // shortcode code name : [good_image name="" url=""]

    // get code
    public function getCode():string{
        return $this->code;
    }

    // controller & renderer
    public function render($attrs):string{
        $arr = [
            'url' => explode(',',$attrs['url']),
            'name' => $attrs['name'],
            'id' => 'C_'.md5($attrs['url']) // ? time() ? uniqid() ? ...
        ];
        $view = view('shortcode.good_image',$arr);
        return $view->render();
    }
}
