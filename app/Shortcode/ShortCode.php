<?php
namespace App\Shortcode;

interface Shortcode{

    public function getCode():string; // Shortcode Code name Ex: good_image / good_text

    public function render(array $attrs):string; // Shortcode Controller & Renderer Method

}