<?php

namespace App\Providers;

use App\Shortcode\List\GoodImage;
use App\Shortcode\List\GoodText;
use App\Shortcode\ShortcodeManager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        ShortcodeManager::register("good_image",new GoodImage());
        ShortcodeManager::register("good_text",new GoodText());
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
